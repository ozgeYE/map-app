import {Component, Inject, NgZone, OnInit, PLATFORM_ID} from '@angular/core';

// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4chartsMaps from '@amcharts/amcharts4/maps';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import {isPlatformBrowser} from "@angular/common";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent {
  private chart: any;

  // @ts-ignore
  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone) {

  }

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      //am4core.useTheme(am4themes_animated);

      let chart = am4core.create("chartdiv", am4chartsMaps.MapChart);
      chart.geodata = am4geodata_worldLow;

      // Set projection
      chart.projection = new am4chartsMaps.projections.Miller();

// Create map polygon series
      let polygonSeries = chart.series.push(new am4chartsMaps.MapPolygonSeries());

// Make map load polygon (like country names) data from GeoJSON
      polygonSeries.useGeodata = true;

// Configure series
      let polygonTemplate = polygonSeries.mapPolygons.template;
      polygonTemplate.tooltipText = "{name}";
      polygonTemplate.fill = am4core.color("#74B266");

// Create hover state and set alternative fill color
      var hs = polygonTemplate.states.create("hover");
      hs.properties.fill = am4core.color("#367B25");

// Remove Antarctica
      polygonSeries.exclude = ["AQ"];

// Add some data
      polygonSeries.data = [{
        "id": "US",
        "name": "United States",
        "value": 100,
        "fill": am4core.color("#F05C5C")
      }, {
        "id": "FR",
        "name": "France",
        "value": 50,
        "fill": am4core.color("#5C5CFF")
      }];

// Bind "fill" property to "fill" key in data
      polygonTemplate.propertyFields.fill = "fill";



    });
  }

}
